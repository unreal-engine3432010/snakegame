// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodThroughObstacle.h"
#include "SnakeBase.h"

// Sets default values
AFoodThroughObstacle::AFoodThroughObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodThroughObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodThroughObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodThroughObstacle::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		Snake->ThroughObstacle = true;
		Snake->ResetThroughObstacleTime();
		Super::Interact(Interactor);
	}
}

