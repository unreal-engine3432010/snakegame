// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameGrid.generated.h"

USTRUCT()
struct FGameCell
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Position;

	UPROPERTY()
	bool IsOccupied;
};

UCLASS()
class SNAKEGAME_API AGameGrid : public AActor
{
	GENERATED_BODY()
	
private:

	UPROPERTY()
	TArray<FGameCell> GameCells;

public:	
	// Sets default values for this actor's properties
	AGameGrid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FGameCell GetGameCell(int _Index);

	void UseGameCell(int _Index, bool _IsOccupied);

	int GetGameCellsNum();

	void ResetGameGrid();
};
