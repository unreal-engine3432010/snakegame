// Fill out your copyright notice in the Description page of Project Settings.


#include "GameGrid.h"

// Sets default values
AGameGrid::AGameGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGameGrid::BeginPlay()
{
	Super::BeginPlay();
	
	for (double XPos = -1500; XPos <= 1500; XPos += 100)
	{
		for (double YPos = -1500; YPos <= 1500; YPos += 100)
		{
			GameCells.Add(FGameCell{ FVector{XPos, YPos, 50}, false });
		}
	    
	}
}

// Called every frame
void AGameGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FGameCell AGameGrid::GetGameCell(int _Index)
{
	return GameCells[_Index];
}

void AGameGrid::UseGameCell(int _Index, bool Use)
{
	GameCells[_Index].IsOccupied = Use;
}

int AGameGrid::GetGameCellsNum()
{
	return GameCells.Num();
}

void AGameGrid::ResetGameGrid()
{
	for (int i = 0; i < GameCells.Num(); i++)
	{
		GameCells[i].IsOccupied = false;
	}
}

