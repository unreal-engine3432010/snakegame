// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodSpawner.generated.h"


class AFood;
class AGameGrid;
class APlayerPawnBase;

UCLASS()
class SNAKEGAME_API AFoodSpawner : public AActor
{
	GENERATED_BODY()
	

private:

	UPROPERTY()
	APlayerPawnBase* PlayerPawn;
	
	UPROPERTY()
	TArray<AFood*> FoodObjects;

public:	
	// Sets default values for this actor's properties
	AFoodSpawner();

	UPROPERTY()
	AGameGrid* GameGrid;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<AFood>> FoodActorClasses;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnFood(int FoodNum = 1);

	void ReleaseGameCell(int _Index);

	void DestroyFood(AFood* Food);

	void DestroyAllFood();
};
