// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodExtraPoints.h"
#include "SnakeBase.h"

// Sets default values
AFoodExtraPoints::AFoodExtraPoints()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodExtraPoints::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodExtraPoints::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodExtraPoints::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		Snake->AddPoints(4);
		Snake->AddSnakeElement(false, 4);
		Super::Interact(Interactor);
	}
}

