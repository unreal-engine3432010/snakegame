// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "GameGrid.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementSpeed = 0.5f;
	ElementSize = 100.f;
	LastMovementDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	//AddSnakeElement(true, 5);
	SetActorTickInterval(MovementSpeed);
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsDead || !IsMoving)
	{
		return;
	}

	if (Health > 0)
	{
		float DeltaHealth = 5 * DeltaTime;
		Health -= DeltaHealth;
	}
	else
	{
		IsDead = true;
		IsMoving = false;
		return;
	}

	if (ThroughObstacle)
	{
		ThroughObstacleTime += DeltaTime;
		if (ThroughObstacleTime >= 15)
		{
			ThroughObstacle = false;
			ThroughObstacleTime = 0;
		}
	}

	
	
		Move();
	
	
}

void ASnakeBase::AddSnakeElement(bool FirstElements, int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; i++)
	{   
		FVector NewLocation{ 0, 0, 0 };
		int CurrentIndex = 0;
		TSubclassOf<ASnakeElementBase> ElementClass;

		if (FirstElements)
		{
			int CentrIndex = (GameGrid->GetGameCellsNum() - 1) / 2;
			CurrentIndex = CentrIndex - (i * 31);
			NewLocation = FVector{ GameGrid->GetGameCell(CurrentIndex).Position };
			GameGrid->UseGameCell(CurrentIndex, true);
			
			
		}
		else
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
			CurrentIndex = SnakeElements[SnakeElements.Num() - 1]->CurrentGameCellIndex;
			
		}

		if (FirstElements && i == 0)
		{
			ElementClass = SnakeHeadClass;
		}
		else
		{
			ElementClass = SnakeElementClass;
		}

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(ElementClass, NewTransform);
		
		SnakeElement->SnakeOwner = this;
		SnakeElement->CurrentGameCellIndex = CurrentIndex;

		int32 ElementIndex = SnakeElements.Add(SnakeElement);

		if (ElementIndex == 0)
		{
			SnakeElement->SetFirstElementType();
		}

	}

	
}

void ASnakeBase::SetDirection(EMovementDirection Direction)
{
	if (!DirectionIsChanged)
	{
		LastMovementDirection = Direction;
		DirectionIsChanged = true;
	}
}

EMovementDirection ASnakeBase::GetDirection()
{
	return LastMovementDirection;
}



void ASnakeBase::Move()
{
	
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		if (i == SnakeElements.Num() - 1)
		{
			GameGrid->UseGameCell(CurrentElement->CurrentGameCellIndex, false);
		}
		
		auto PrevElement = SnakeElements[i - 1];
		CurrentElement->CurrentGameCellIndex = PrevElement->CurrentGameCellIndex;
		CurrentElement->SetActorLocation(PrevElement->GetActorLocation());
		CurrentElement->SetActorRotation(PrevElement->GetActorRotation());
	}
	
	
	
	FVector MoveLocation = SnakeElements[0]->GetActorLocation();
	FRotator Rotation{ 0, 0, 0 };
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		SnakeElements[0]->CurrentGameCellIndex += 31;
		MoveLocation.X += 100;
		Rotation.Yaw = 0;
			break;

	case EMovementDirection::DOWN:
		SnakeElements[0]->CurrentGameCellIndex -= 31;
		MoveLocation.X -= 100;
		Rotation.Yaw = 180;
		break;

	case EMovementDirection::RIGHT:
		SnakeElements[0]->CurrentGameCellIndex++;
		MoveLocation.Y += 100;
		Rotation.Yaw = 90;
		break;

	case EMovementDirection::LEFT:
		SnakeElements[0]->CurrentGameCellIndex --;
		MoveLocation.Y -= 100;
		Rotation.Yaw = -90;
		break;
	}

	
	int GameCellIndex = SnakeElements[0]->CurrentGameCellIndex;
	SnakeElements[0]->SetActorLocation(MoveLocation, true);
	SnakeElements[0]->SetActorRotation(Rotation);

	if (GameCellIndex < GameGrid->GetGameCellsNum() && GameCellIndex > 0)
	{
		GameGrid->UseGameCell(GameCellIndex, true);
	}
	

	

	SnakeElements[0]->ToggleCollision();

	DirectionIsChanged = false;

}

void ASnakeBase::SnakeElementOverlapped(AActor* OtherActor)
{
	
	auto Interactable = Cast<IInteractable>(OtherActor);
	if (Interactable) 
	{
		Interactable->Interact(this);
	}
	else
	{
		auto ParentActor = OtherActor->GetAttachParentActor();
		Interactable = Cast<IInteractable>(ParentActor);
		if (Interactable)
		{
			Interactable->Interact(this);
		}
	}
}

void ASnakeBase::IncreaseSpeed()
{
	if (MovementSpeed <= 0.05f)
		return;

	MovementSpeed -= 0.05f;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::DecreaseSpeed()
{
	MovementSpeed += 0.05f;
	SetActorTickInterval(MovementSpeed);
}

FVector ASnakeBase::GetHeadPosition()
{
	return SnakeElements[0]->GetActorLocation();
}

void ASnakeBase::RestoreHealth()
{
	Health = 100;
}

void ASnakeBase::AddPoints(int Points)
{
	Score += Points;
}

void ASnakeBase::ResetThroughObstacleTime()
{
	ThroughObstacleTime = 0;
}

float ASnakeBase::GetHealth()
{
	return Health;
}

int ASnakeBase::GetScore()
{
	return Score;
}

void ASnakeBase::DestroySnake()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}

	Destroy();
}

