// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ObstacleSpawner.generated.h"

class ASpawnableObstacle;
class AGameGrid;
class APlayerPawnBase;


UCLASS()
class SNAKEGAME_API AObstacleSpawner : public AActor
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
	TArray<ASpawnableObstacle*> SpawnedObstacles;

public:	
	// Sets default values for this actor's properties
	AObstacleSpawner();

	
	UPROPERTY()
	AGameGrid* GameGrid;

	UPROPERTY()
	APlayerPawnBase* PlayerPawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpawnableObstacle> ObstacleClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor> ChildActorClass;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnObstacles(int ObstaclesNum = 1);

	void ReleaseGameCell(int _Index);

	void DestroyObstacle(ASpawnableObstacle* Obstacle);

	void DestroyObstacles();
};
