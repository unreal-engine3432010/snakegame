// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AGameManager;
class AGameGrid;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

private:

	UPROPERTY()
	ASnakeBase* SnakeActor;

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();


	UPROPERTY()
	AGameManager* GameManager = nullptr;

private:

	UPROPERTY()
	UCameraComponent* PawnCamera;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor(AGameGrid* _GameGrid);
	

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION()
	void HandlePlayerSpaceButtonPressed();

	UFUNCTION(BlueprintCallable)
	bool IsSnakeDead();

	UFUNCTION(BlueprintCallable)
	bool IsSnakeMoving();

	FVector GetSnakeHeadPosition();

	UFUNCTION(BlueprintCallable)
	float GetSnakeHealth();

	UFUNCTION(BlueprintCallable)
	int GetSnakeScore();

};
