// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Obstacle.h"
#include "SpawnableObstacle.generated.h"

class AObstacleSpawner;

UCLASS()
class SNAKEGAME_API ASpawnableObstacle : public AObstacle
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	ASpawnableObstacle();

	TArray<int> GameCellIndexes;

	UPROPERTY()
	AObstacleSpawner* ObstacleSpawner = nullptr;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	float LifeTime = FMath::RandRange(7.0f, 15.0f);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor) override;
};
