// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "GameManager.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Space Button", EInputEvent::IE_Pressed, this, &APlayerPawnBase::HandlePlayerSpaceButtonPressed);
}

void APlayerPawnBase::CreateSnakeActor(AGameGrid* _GameGrid)
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->DestroySnake();
	}
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->GameGrid = _GameGrid;
	SnakeActor->AddSnakeElement(true, 5);
}



void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor-> GetDirection() != EMovementDirection::DOWN)
			SnakeActor->SetDirection(EMovementDirection::UP);
		else if(value < 0 && SnakeActor->GetDirection() != EMovementDirection::UP)
			SnakeActor->SetDirection(EMovementDirection::DOWN);
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->GetDirection() != EMovementDirection::LEFT)
			SnakeActor->SetDirection(EMovementDirection::RIGHT);
		else if(value < 0 && SnakeActor->GetDirection() != EMovementDirection::RIGHT)
			SnakeActor->SetDirection(EMovementDirection::LEFT);
	}
}

void APlayerPawnBase::HandlePlayerSpaceButtonPressed()
{
	if (IsValid(SnakeActor))
	{
		if (!SnakeActor->IsMoving)
		{
			GameManager->StartGame();
			
		}
	}
	else
	{
		GameManager->StartGame();
	}
	
}

bool APlayerPawnBase::IsSnakeDead()
{
	if (IsValid(SnakeActor))
	{
		return SnakeActor->IsDead;
	}
	
	return false;
}

bool APlayerPawnBase::IsSnakeMoving()
{
	if (IsValid(SnakeActor))
	{
		return SnakeActor->IsMoving;
	}
	
	return false;
}

FVector APlayerPawnBase::GetSnakeHeadPosition()
{
	return SnakeActor->GetHeadPosition();
}

float APlayerPawnBase::GetSnakeHealth()
{
	if (IsValid(SnakeActor))
	{
		return SnakeActor->GetHealth();
	}

	return 0;
}

int APlayerPawnBase::GetSnakeScore()
{
	if (IsValid(SnakeActor))
	{
		return SnakeActor->GetScore();
	}
	
	return 0;
}







