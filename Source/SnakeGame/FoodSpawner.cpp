// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"
#include "Food.h"
#include "GameGrid.h"
#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"



// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	PlayerPawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void AFoodSpawner::SpawnFood(int FoodNum)
{
	if (PlayerPawn->IsSnakeDead())
	{
		return;
	}

	bool DoNotSpawn = false;

	for (int i = 0; i < FoodNum; i++)
	{
		int RandomIndex = FMath::RandRange(0, FoodActorClasses.Num() - 1);
		auto FoodActorClass = FoodActorClasses[RandomIndex];
		FVector SpawnLocation;
		FGameCell GameCell;
		int GameCellIndex = 0;
		int Attempts = 0;

		do
		{
			if (Attempts > 100)
			{
				DoNotSpawn = true;
				break;
			}
			GameCellIndex = FMath::RandRange(0, GameGrid->GetGameCellsNum() - 1);
			GameCell = GameGrid->GetGameCell(GameCellIndex);
			SpawnLocation = GameCell.Position;
			Attempts++;
		} while (GameCell.IsOccupied);

		if (DoNotSpawn)
		{
			break;
		}
		auto Food = GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform(SpawnLocation));
		GameGrid->UseGameCell(GameCellIndex, true);
		Food->FoodSpawner = this;
		Food->GameCellIndex = GameCellIndex;
		FoodObjects.Add(Food);
	}
}

void AFoodSpawner::ReleaseGameCell(int _Index)
{
	GameGrid->UseGameCell(_Index, false);
}

void AFoodSpawner::DestroyFood(AFood* Food)
{
	FoodObjects.Remove(Food);
	Food->Destroy();
	SpawnFood();
}

void AFoodSpawner::DestroyAllFood()
{
	for (int i = 0; i < FoodObjects.Num(); i++)
	{
		FoodObjects[i]->Destroy();
	}

	FoodObjects.Empty();
}





