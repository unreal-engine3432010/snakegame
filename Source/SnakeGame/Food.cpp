// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "FoodSpawner.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	LifeTime -= DeltaTime;

	if (LifeTime <= 0)
	{
		FoodSpawner->ReleaseGameCell(GameCellIndex);
		FoodSpawner->DestroyFood(this);
	}

	
}

void AFood::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->AddSnakeElement(false);
		Snake->RestoreHealth();
		Snake->AddPoints(1);
		FoodSpawner->ReleaseGameCell(GameCellIndex);
		FoodSpawner->DestroyFood(this);
	}
}

