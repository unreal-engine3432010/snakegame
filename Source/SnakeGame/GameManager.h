// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameManager.generated.h"



class AFoodSpawner;
class AObstacleSpawner;
class AGameGrid;
class APlayerPawnBase;



UCLASS()
class SNAKEGAME_API AGameManager : public AGameModeBase
{
	GENERATED_BODY()

private:

	UPROPERTY()
	AGameGrid* GameGrid;

	UPROPERTY()
	APlayerPawnBase* PlayerPawn;

	UPROPERTY()
	AFoodSpawner* FoodSpawner = nullptr;

	UPROPERTY()
	AObstacleSpawner* ObstacleSpawner = nullptr;

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodSpawner> FoodSpawnerClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacleSpawner> ObstacleSpawnerClass;

	

public:

	virtual void StartPlay() override;

	virtual void Tick(float DeltaTime) override;

	void InitiateGame();

	void StartGame();
	
};
