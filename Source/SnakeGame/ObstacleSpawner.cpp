// Fill out your copyright notice in the Description page of Project Settings.


#include "ObstacleSpawner.h"
#include "GameGrid.h"
#include "SpawnableObstacle.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"




// Sets default values
AObstacleSpawner::AObstacleSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AObstacleSpawner::BeginPlay()
{
	Super::BeginPlay();
	PlayerPawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));
}

// Called every frame
void AObstacleSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AObstacleSpawner::SpawnObstacles(int ObstaclesNum)
{
	
	for (int i = 0; i < ObstaclesNum; i++)
	{
		if (PlayerPawn->IsSnakeDead())
		{
			return;
		}

		
		ASpawnableObstacle* Obstacle = nullptr;
		FGameCell GameCell;
		int GameCellIndex = 0;
		FVector BlockLocation{ 0, 0, 0 };
		bool CloseToSnakeHead = false;
		bool OutOfGameGrid = false;
		bool OutOfArray = false;
		bool GameCellIsOccupied = false;
		auto HeadPosition = PlayerPawn->GetSnakeHeadPosition();
		int BlocksCount = FMath::RandRange(1, 5);
		

		for (int j = 0; j < BlocksCount; j++)
		{
			TArray<int> Directions{ 1, 2, 3, 4 };
			
			int PrevGameCellIndex = 0;
			FVector PrevSpawnLocation{ 0, 0, 0 };

			bool DoNotSpawn = false;

			int Attempts = 0;
			do
			{
				if (Attempts > 100)
				{
					DoNotSpawn = true;
					break;
				}

				if (j == 0)
				{
					GameCellIndex = FMath::RandRange(0, GameGrid->GetGameCellsNum() - 1);
					Attempts++;
				}
				else
				{
					
					
					if (Directions.Num() == 0)
					{
						DoNotSpawn = true;
						break;
					}

					if (Directions.Num() == 4)
					{
						PrevGameCellIndex = GameCellIndex;
						PrevSpawnLocation = BlockLocation;
					}
					else
					{
						GameCellIndex = PrevGameCellIndex;
						BlockLocation = PrevSpawnLocation;
					}

					
				
						
						int DirectionsNum = Directions.Num();
						int Index = FMath::RandRange(0, Directions.Num() - 1);
						
						int Direction = Directions[Index];
						switch (Direction)
						{
						case 1:
								GameCellIndex += 31;
								BlockLocation.X += 100;
								
							break;
						case 2:
							
								GameCellIndex -= 31;
								BlockLocation.X -= 100;
								
							
							
							break;
						case 3:
							
								GameCellIndex++;
								BlockLocation.Y += 100;
								
							
							break;
						case 4:
							
								GameCellIndex--;
								BlockLocation.Y -= 100;
								
							
							break;
						}

						Directions.RemoveAt(Index);
				}
					
				

				
				if (GameCellIndex > 0 && GameCellIndex < GameGrid->GetGameCellsNum())
				{
					OutOfArray = false;
					GameCell = GameGrid->GetGameCell(GameCellIndex);
					
					GameCellIsOccupied = GameCell.IsOccupied;
					FVector Distance = (GameCell.Position - HeadPosition).GetAbs();
					if (Distance.X <= 200 || Distance.Y <= 200)
					{
						CloseToSnakeHead = true;
					}
					else
					{
						CloseToSnakeHead = false;
					}

					if (IsValid(Obstacle))
					{
						if (Obstacle->GetActorLocation() != GameCell.Position - BlockLocation)
						{
							
							OutOfGameGrid = true;
						}
						else
						{
							OutOfGameGrid = false;
						}
					}
					
					else
					{
						
						OutOfGameGrid = false;
					}
				}
				else
				{
					OutOfArray = true;
				}
				
			} while (GameCellIsOccupied || OutOfGameGrid || CloseToSnakeHead || OutOfArray);

			if (DoNotSpawn)
			{
		
				break;
			}

			
			if (!IsValid(Obstacle))
			{
			  Obstacle = GetWorld()->SpawnActor<ASpawnableObstacle>(ObstacleClass, FTransform(GameCell.Position));
			  Obstacle->ObstacleSpawner = this;
			  SpawnedObstacles.Add(Obstacle);
			}
			
		auto ActorComponent =	Obstacle->AddComponentByClass(UChildActorComponent::StaticClass(), false, FTransform{ BlockLocation }, false);
		auto ChildActorComponent = Cast<UChildActorComponent>(ActorComponent);
		 ChildActorComponent->SetChildActorClass(ChildActorClass);
		GameGrid->UseGameCell(GameCellIndex, true);
		Obstacle->GameCellIndexes.Add(GameCellIndex);
				
		}
	}
}

void AObstacleSpawner::ReleaseGameCell(int _Index)
{
	GameGrid->UseGameCell(_Index, false);
}

void AObstacleSpawner::DestroyObstacle(ASpawnableObstacle* Obstacle)
{
	SpawnedObstacles.Remove(Obstacle);
	Obstacle->Destroy();
	SpawnObstacles();
}

void AObstacleSpawner::DestroyObstacles()
{
	for (int i = 0; i < SpawnedObstacles.Num(); i++)
	{
		SpawnedObstacles[i]->Destroy();
	}

	SpawnedObstacles.Empty();
}






