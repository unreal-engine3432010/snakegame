// Fill out your copyright notice in the Description page of Project Settings.


#include "GameManager.h"
#include "FoodSpawner.h"
#include "ObstacleSpawner.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "GameGrid.h"


void AGameManager::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGameManager::StartPlay()
{
	Super::StartPlay();
	InitiateGame();
	
}

void AGameManager::Tick(float DeltaTime)
{
	
}

void AGameManager::InitiateGame()
{
	GameGrid = GetWorld()->SpawnActor<AGameGrid>();
	PlayerPawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(this, 0));
	PlayerPawn->GameManager = this;
	FoodSpawner = GetWorld()->SpawnActor<AFoodSpawner>(FoodSpawnerClass);
	ObstacleSpawner = GetWorld()->SpawnActor<AObstacleSpawner>(ObstacleSpawnerClass);
	FoodSpawner->GameGrid = GameGrid;
	ObstacleSpawner->GameGrid = GameGrid;
}

void AGameManager::StartGame()
{
	GameGrid->ResetGameGrid();
	FoodSpawner->DestroyAllFood();
	ObstacleSpawner->DestroyObstacles();
	PlayerPawn->CreateSnakeActor(GameGrid);
	FoodSpawner->SpawnFood(5);
	ObstacleSpawner->SpawnObstacles(5);
}
