// Fill out your copyright notice in the Description page of Project Settings.


#include "IncreaseSpeed.h"
#include "SnakeBase.h"

// Sets default values
AFoodIncreaseSpeed::AFoodIncreaseSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodIncreaseSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodIncreaseSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodIncreaseSpeed::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		Snake->IncreaseSpeed();
		Super::Interact(Interactor);
	}
}

