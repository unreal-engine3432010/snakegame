// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;
class AGameGrid;

UENUM()
enum class EMovementDirection : uint8 
{
	UP,
	DOWN,
	RIGHT,
	LEFT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()

private:

	bool DirectionIsChanged = 0;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	

	float ThroughObstacleTime = 0;
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY()
	EMovementDirection LastMovementDirection;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeHeadClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	
	UPROPERTY()
	AGameGrid* GameGrid = nullptr;

	bool ThroughObstacle = false;
	
	bool IsDead = false;
	
	bool IsMoving = true;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	float Health = 100;

	UPROPERTY(BlueprintReadOnly)
	int Score = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(bool FirstElements, int ElementsNum = 1);

	void SetDirection(EMovementDirection Direction);

	EMovementDirection GetDirection();

	void Move();

	void SnakeElementOverlapped(AActor* OtherActor);

	void IncreaseSpeed();

	void DecreaseSpeed();

	FVector GetHeadPosition();

	void RestoreHealth();

	void AddPoints(int Points);

	void ResetThroughObstacleTime();

	float GetHealth();

	int GetScore();

	void DestroySnake();
};
