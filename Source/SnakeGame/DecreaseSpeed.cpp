// Fill out your copyright notice in the Description page of Project Settings.


#include "DecreaseSpeed.h"
#include "SnakeBase.h"

// Sets default values
AFoodDecreaseSpeed::AFoodDecreaseSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodDecreaseSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodDecreaseSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodDecreaseSpeed::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		
		Snake->DecreaseSpeed();
		Super::Interact(Interactor);
	}
}

