// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnableObstacle.h"
#include "SnakeBase.h"
#include "ObstacleSpawner.h"

// Sets default values
ASpawnableObstacle::ASpawnableObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawnableObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawnableObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	LifeTime -= DeltaTime;

	if (LifeTime <= 0)
	{
		for (int i = 0; i < GameCellIndexes.Num(); i++)
		{
			ObstacleSpawner->ReleaseGameCell(GameCellIndexes[i]);
			
		}

		ObstacleSpawner->DestroyObstacle(this);
		
	}
}

void ASpawnableObstacle::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (!Snake->ThroughObstacle)
		{
			Super::Interact(Interactor);
		}
		
	}
}

